import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Image
} from "react-native";
import React, {useState, useRef, useEffect, useMemo, useCallback} from 'react';

import Icon from 'react-native-vector-icons/Ionicons';
import IconFA5 from 'react-native-vector-icons/FontAwesome5';
import Map, { Marker } from 'react-native-maps';
import Geolocation from "@react-native-community/geolocation";
import BottomSheet from "@gorhom/bottom-sheet";
import { GestureHandlerRootView } from "react-native-gesture-handler";

export const Home = ({navigation}) => {
    const [region, setRegion] = useState({
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
    });
    const locations = [
        {
            latitude: 41.423310, 
            longitude: 2.182943
        },
        {
            latitude: 41.423310, 
            longitude: 2.192943
        },
        {
            latitude: 41.423310, 
            longitude: 2.186943
        },
        {
            latitude: 41.423310, 
            longitude: 2.194943
        },
    ];
    const bottomSheetRef = useRef(null);
    const snapPoints = useMemo(() => ['13%', '35%', '90%'], []);

    const handleSheetChanges = useCallback((index) => {
        console.log('sheetChanges', index);
    }, []);

    const getCurrentLocation = () => {
        Geolocation.getCurrentPosition(info => setRegion({
            latitude: info.coords.latitude,
            longitude: info.coords.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        }));
    }

    useEffect(() => {
        getCurrentLocation();
        return () => {}
    }, []);
    useEffect(() => {
        navigation.addListener('beforeRemove', (e) =>{
            e.preventDefault();
        });
    }, [navigation])

    return(
        <GestureHandlerRootView style={style.container}>
            <View style={style.floatingMapButtons}>
                <TouchableOpacity
                    activeOpacity={0.8}
                >
                    <View style={style.filterButton}>
                        <Icon name="filter" size={30} color="#2c3e50" />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={getCurrentLocation}
                >
                    <View style={style.locationButton}>
                        <IconFA5 name="location-arrow" size={30} color="#2c3e50" />
                    </View>
                </TouchableOpacity>
            </View>
            <Map
                region={region}
                style={style.map}
                showsUserLocation={true}
                scrollEnabled={true}
            >
                {
                    locations.map((marker, index) => (
                        <Marker 
                            key={index}
                            coordinate={{latitude: marker.latitude, longitude: marker.longitude}}
                        >
                            <TouchableOpacity>
                                <View style={style.containerCustomMarker}>
                                    <Image
                                        source={require('../assets/electric-scooter.png')}
                                        style={style.rideImage}
                                    />
                                </View>
                            </TouchableOpacity>
                        </Marker>
                    ))
                }
            </Map>
            <BottomSheet
                ref={bottomSheetRef}
                index={1}
                snapPoints={snapPoints}
                onChange={handleSheetChanges}
            >
                <View style={style.bottomSheetContainer}>
                    <TouchableOpacity
                        style={style.button}
                    >
                        <View style={style.buttonContainer}>
                            <Icon name="scan" size={30} color="#fff" />
                            <Text style={style.textButtonScanner}> Escanear vehículo</Text>
                        </View>
                    </TouchableOpacity>
                    <Text style={style.menuText}>Menú</Text>
                    <View style={style.apartadoFondos}>
                        <TouchableOpacity
                            style={style.cartera}
                        >
                            <View style={style.carteraContainer}>
                                <Text style={style.title}>Cartera</Text>
                                <Text style={style.fondos}>0€</Text>
                                <View style={style.containerAdd}>
                                    <Text style={style.textAdd}>Añadir</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableWithoutFeedback
                            style={style.pases}
                        >
                            <View style={style.carteraContainer}>
                                <Text>Pases</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                    <View style={style.codePromoContainer}>
                        <Text style={style.codeText}>Código promocional</Text>
                        <TouchableOpacity style={style.addCodeButton}>
                            <View>
                                <Text style={style.textAdd}>Añadir</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity>
                        <View style={style.helpCenterContainer}>
                            <Text style={style.helpCenterText}>Centro de ayuda</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </BottomSheet>
        </GestureHandlerRootView>
    )
}

const style = StyleSheet.create({
    container:{
        flex: 1
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    marker: {
        backgroundColor: '#fff'
    },
    rideImage: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain'
    },
    containerCustomMarker: {
        width: 40,
        height: 40,
        borderRadius: 36,
        backgroundColor: '#fff',
        padding: 10,
        justifyContent: 'center',
        alignContent: 'center',
        flex: 1
    },
    button: {
        backgroundColor: '#2c3e50',
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 20,
        padding: 10
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent:'center',
        alignContent: 'center'
    },
    bottomSheetContainer: {
        padding: 15,
        backgroundColor: '#f7f7f7',
        flex: 1
    },
    textButtonScanner: {
        color: '#fff',
        fontSize: 18
    },
    menuText: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#000',
        marginTop: 15
    },
    apartadoFondos: {
        width: '100%',
        marginTop: 15,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    cartera: {
        width: '45%',
        backgroundColor: '#fff',
        borderRadius: 15,
        padding: 10
    },
    title:{
        color: '#000',
        fontSize: 16,
        fontWeight: 'bold'
    },
    fondos: {
        fontSize: 44
    },
    containerAdd:{
        backgroundColor: '#a29bfe',
        width: '60%',
        padding: 2,
        borderRadius: 6
    },
    textAdd: {
        textAlign: 'center',
        color: '#6c5ce7',
        fontWeight: 'bold'
    },
    pases: {
        width: '45%',
    },
    codePromoContainer: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        padding: 15,
        marginTop: 15,
        borderRadius: 7,
        justifyContent: 'space-between'
    },
    codeText: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 15
    },
    addCodeButton: {
        backgroundColor: '#a29bfe',
        width: '30%',
        padding: 2,
        borderRadius: 6
    },
    helpCenterContainer: {
        backgroundColor: '#fff',
        marginTop: 15,
        padding: 15,
        borderRadius: 7
    },
    helpCenterText: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 15
    },
    floatingMapButtons: {
        position: 'absolute',
        top: 11,
        right: 11,
        zIndex: 10,
        flexDirection: 'column',
        flex: 1
    },
    filterButton: { 
        backgroundColor: '#fff',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        borderBottomWidth: 1,
        padding: 10
    },
    locationButton: {
        backgroundColor: '#fff',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        padding: 10
    }
});