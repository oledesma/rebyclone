import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TouchableWithoutFeedback
} from "react-native";
import React, {useState, useRef, useEffect} from 'react';

import Icon from 'react-native-vector-icons/Ionicons';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import auth from '@react-native-firebase/auth';

export const SMSverification = ({route, navigation}) => {
    //Parámetros recibidos del navegador
    const {number} = route.params;
    //Inicialización de variables
    const [code, setCode] = useState('');
    const [counter, setCounter] = useState(60);
    const [confirm, setConfirm] = useState();
    //Handlers
    const handleBackPress = () => navigation.goBack();
    const handleChangeCode = (code) => setCode(code);
    const handleConfirmCode = async () => {
        try{
            await confirm.confirm(code);
            navigation.navigate('Home');
        }catch(e) {
            console.log(e)
        }
    }
    const signInWithPhoneNumber = async () => {
        const confirmation = await auth().signInWithPhoneNumber(number);
        setConfirm(confirmation);
    };
    //
    useEffect(() => {
        signInWithPhoneNumber();
        return () => {};
    }, []);
    useEffect(() => {
        if(counter === 0) return;
        setTimeout(() => {
            setCounter(counter - 1);
        }, 1000);
    }, [counter])

    return(
        <View style={style.container}>
            <View>
                <TouchableWithoutFeedback
                    onPress={handleBackPress}
                >
                    <View>
                        <Icon name="arrow-back" size={30} color="#000" />
                    </View>
                </TouchableWithoutFeedback>
                <Text style={style.title}>Comprobar el código</Text>
                <Text style={style.description}>
                    Añade el código que te enviamos por SMS a {number}
                </Text>
                <SmoothPinCodeInput
                    cellStyle={style.cellStyle}
                    cellStyleFocused={style.cellStyleFocused}
                    value={code}
                    onTextChange={handleChangeCode}
                    codeLength={6}
                    cellSpacing={12}
                    onFullfill={handleConfirmCode}
                />
            </View>
            {
                counter !== 0 ?
                <Text style={style.textRetry}>Podrás solicitar un nuevo código en {counter}</Text>
                :
                <TouchableWithoutFeedback
                    onPress={signInWithPhoneNumber}
                >
                    <View>
                        <Text style={style.resend}>Reenviar código de confirmación</Text>
                    </View>
                </TouchableWithoutFeedback>
            }
        </View>
    );
} 

const style = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignContent: 'center',
        padding: 15,
        backgroundColor: '#f7f7f7'
    },
    title: {
        fontSize: 34,
        fontWeight: 'bold',
        color: '#000',
        marginTop: 20
    },
    description: {
        fontSize: 20
    },
    cellStyle: {
        backgroundColor: '#fff',
        borderRadius: 15,
        width: 50,
        height: 70,
        marginTop: 60
    },
    cellStyleFocused: {
        borderWidth: 1,
        borderColor: '#2c3e50'
    },
    textRetry: {
        fontSize: 20,
        textAlign: 'center'
    },
    resend: {
        textAlign: 'center',
        fontSize: 16,
        fontWeight: 'bold',
        color: '#000'
    }
});