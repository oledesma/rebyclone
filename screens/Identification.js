import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Keyboard
} from "react-native";
import React, {useState, useRef, useMemo, useCallback, useEffect} from 'react';
/**
 * Librerias utilizadas para utilizar input con validación de teléfono
 * @link https://github.com/garganurag893/react-native-phone-number-input
 * @link https://github.com/gorhom/react-native-bottom-sheet
 */
import PhoneInput, { isValidNumber } from "react-native-phone-number-input";
import {BottomSheetModalProvider, BottomSheetModal} from "@gorhom/bottom-sheet";
//Gesture Handler para controlar los gestos del usuario.
import { GestureHandlerRootView } from "react-native-gesture-handler";
import auth from '@react-native-firebase/auth';

export const Identification = ({navigation}) => {
    //##Inicialización de variables
    const [buttonDisabled, setButtonDisabled] = useState(true);
    const [value, setValue] = useState('');
    const phoneInput = useRef<PhoneInput>(null);
    const bottomSheetModalRef = useRef(null);
    const snapPoints = useMemo(() => ['25%', '60%'], []);
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();
    //##Props de touchable Opacity
    let touchProps = {
        style: buttonDisabled ? style.buttonDisabled : style.button,
        disabled: buttonDisabled,
    };
    //Handler que valida el número introducido, el cual utiliza de su propia libreria
    const validateNumber = (number) => { 
        setValue(number);
        if(isValidNumber(number, 'ES')) setButtonDisabled(false);
        else setButtonDisabled(true);
    }
    const onAuthStateChange = (user) => {
        setUser(user);
        if(user) navigation.navigate('Home');
    };
    //Handlers para el control de los eventos del Modal Sheet.
    const handleClosePress = () => {
        bottomSheetModalRef.current.close();
        navigation.navigate('SMSverification', {number: value}); //navegar a verificación SMS y pasando el número.
    }
    const handleSheetChanges = useCallback((index) => {
        console.log('handleSheetChanges', index);
        console.log(bottomSheetModalRef)
    }, []);
    const handleModalSheetPresent = useCallback(() => {
        console.log(bottomSheetModalRef);
        Keyboard.dismiss();
        bottomSheetModalRef.current?.present();
    }, []);

    useEffect(() => {
        const suscriber = auth().onAuthStateChanged(onAuthStateChange);
        return suscriber;
    }, []);
    
    return (
        <GestureHandlerRootView style={style.container}>
            <BottomSheetModalProvider>
                <View style={style.phone}>
                    <Text style={style.title}>Teléfono móvil</Text>
                    <Text style={style.description}>
                        Te enviaremos un código via SMS para validar tu cuenta.
                    </Text>
                    <PhoneInput
                        useRef={phoneInput}
                        containerStyle={style.phoneInput}
                        defaultValue={value}
                        defaultCode="ES"
                        placeholder="Teléfono móvil"
                        onChangeFormattedText={validateNumber}
                        layout='first'
                        withDarkTheme
                        withShadow
                        autoFocus
                    />
                </View>
                <TouchableOpacity
                    {...touchProps}
                    onPress={handleModalSheetPresent}
                >
                    <Text style={buttonDisabled ? style.textButtonDisabled : style.textButton}>Continuar</Text>
                </TouchableOpacity>
                <BottomSheetModal
                    ref={bottomSheetModalRef}
                    index={1}
                    snapPoints={snapPoints}
                    onChange={handleSheetChanges}
                >
                    <View style={style.contentContainer}>
                        <Text style={style.titleTerminos}>Términos y condiciones</Text>
                        <Text style={style.descripcionTerminos}>
                            Reby Rides, SL es el responsable de los datos personales que nos facilitas para ofrecerte
                            nuestro servicio de movilidad. Para saber cómo ejercitar tus derechos de toda la información
                            sobre los tratamientos que realizamos, puedes dirigirte a nuestro .
                        </Text>
                        <Text style={style.colorText}>
                            He leído y acepto los
                            <Text style={style.linkTerminos}>
                                Términos y condiciones 
                            </Text>
                            y la
                            <Text style={style.linkTerminos}>
                                Política de privacidad.
                            </Text>
                        </Text>
                        <TouchableOpacity
                            onPress={handleClosePress}
                            style={style.button}
                        >
                            <Text style={style.textButton}>Continuar</Text>
                        </TouchableOpacity>
                    </View>
                </BottomSheetModal>
            </BottomSheetModalProvider>
        </GestureHandlerRootView>
    );
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        padding: 10
    },
    phone: {
        paddingTop: 20
    },
    title: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#000'
    },  
    description:{
        fontSize: 17
    },
    phoneInput: {
        width: '100%',
        marginTop: 10
    },
    button: {
        backgroundColor: '#2c3e50',
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 20,
        padding: 10
    },
    buttonDisabled: {
        backgroundColor: '#d1ccc0',
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 20,
        padding: 10
    },
    textButton: {
        color: '#fff',
        fontSize: 20
    },
    textButtonDisabled: {
        color: '#84817a',
        fontSize: 20
    },
    contentContainer: {
        padding: 15,
        alignContent: 'center',
        justifyContent: 'space-between',
        flex: 1
    },  
    titleTerminos: {
        color: "#000",
        fontSize: 25,
        fontWeight: 'bold'
    },
    descripcionTerminos: {
        color: '#000',
        fontSize: 16,
        textAlign: 'left'
    },
    colorText: {
        color: '#000'
    },
    linkTerminos: {
        color: '#2c3e50',
        fontWeight: 'bold'
    }
});