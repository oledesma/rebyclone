# RebyClone

Es un proyecto realizado para probar mis conocimientos en React Native utilizando hooks. **Está desarrollado solo para Android**.

## Instalación

Primero de todo lo que debes hacer es clonar el repositorio, una vez dentro, asegurate de utilizar el comando de npm para instalar
todas las librerías necesarias.
```
git clone https://gitlab.com/oledesma/rebyclone.git

npm install
```

## Librerías utilizadas

- https://github.com/garganurag893/react-native-phone-number-input
- https://github.com/gorhom/react-native-bottom-sheet
- https://github.com/software-mansion/react-native-gesture-handler
- https://github.com/invertase/react-native-firebase
- https://github.com/oblador/react-native-vector-icons
- https://github.com/xamous/react-native-smooth-pincode-input

